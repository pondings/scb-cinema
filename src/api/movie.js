import axios from 'axios';

export const apiUrl = `http://scb-movies-api.herokuapp.com/api/movies/search`;
export const apiKey = 'e229111ebe082daebfb865dfbcc168d89e3dd210';

export const getHeader = () => ({
    'api-key': apiKey
});

export const fetchMovies = async (wording, page) => new Promise(async (resolve, reject) => {
    try {
        const endpoint = `${apiUrl}?query=${wording}&page=${page}`;
        const response = await axios.get(endpoint, { headers: getHeader() });
        const responseData = response.data;

        return resolve(responseData);
    } catch (error) {
        return reject(`Error fetchMovies: ${error.toString()}`);
    }
});