import * as movieApi from './../movie';
import axios from 'axios';

jest.mock('axios');

describe('Header', () => {

    it('Should populate api-key correctly', () => {
        expect(movieApi.getHeader()).toEqual({ 'api-key': movieApi.apiKey });
    });

});

describe('Fetch Movies', () => {

    it('fetch movies work correctly', async () => {
        const mockData = { total_pages: 2, page: 1, results: [{ title: 'Spider man' }] };

        axios.get.mockResolvedValue({ data: mockData })
        const result = await movieApi.fetchMovies('spider', 1);

        expect(result).toEqual(mockData);
        expect(axios.get).toHaveBeenCalledWith(`${movieApi.apiUrl}?query=spider&page=1`, { headers: {
            'api-key': movieApi.apiKey
        } });
    });

});