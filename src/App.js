import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

/* Components */
import { Header } from './components/Header/Header';
import { Spinner } from './components/Spinner/Spinner';
import { History } from './features/history/History';
import { MovieList } from './features/movieList/MovieList';
import { MovieDetail } from './features/movieDetail/MovieDetail';
import { Favorite } from './features/favorite/Favorite';

/* Styles */
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

/* Bootstrap */
import { Container } from 'react-bootstrap';

const App = () => {
  return (
    <Container className="h-100 px-0 px-md-1">
      <Router>
        <Header />

        <div className="content">
          <Spinner />
          <Switch>
            <Route exact path="/">
              <History />
            </Route>
            <Route path="/movie-list">
              <MovieList />
            </Route>
            <Route path="/movie-detail">
              <MovieDetail />
            </Route>
            <Route path="/favorite">
              <Favorite />
            </Route>
          </Switch>
        </div>
        <Redirect to="/" />
      </Router>
    </Container>
  );
}

export default App;
