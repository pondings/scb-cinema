import * as utils from './../utils';

describe('Utils', () => {

    it('Should concant poster path correctly', () => {

        expect(utils.populatePosterPath({ poster_path: 'spider-man' }))
            .toEqual({ poster_path: `${utils.posterUrl}/spider-man` });

    });

});