import React from 'react';

import Dotdotdot from 'react-dotdotdot';

export const posterUrl = 'https://image.tmdb.org/t/p/w92';

export const buildMovieDescriptRow = (movies = [], handleClick) => movies.map((movie, idx) => (
    <tr key={`${movie.id}-${idx}`} onClick={() => handleClick(movie)}>
        <td>
            <div className="h-100-p w-100-p row">
                <div className="col-4 col-md-1">
                    <img alt="" src={movie.poster_path}></img>
                </div>
                <div className="movie-detail col-8 col-md-11 pl-1 pl-md-5 pl-lg-5 pl-xl-4 d-flex flex-column">
                    <span className="font-weight-bold">{movie.title}</span>
                    <span className="font-weight-light release-data">{movie.release_date}</span>
                    <Dotdotdot clamp={4}>
                        {movie.overview}
                    </Dotdotdot>
                </div>
            </div>
        </td>
    </tr>
));

export const populatePosterPath = (result) => {
    result.poster_path = `${posterUrl}/${result.poster_path}`;   
    return result;
}