import { configureStore } from '@reduxjs/toolkit';
import historyReducer from '../features/history/historySlice';
import movieListReducer from '../features/movieList/movieListSlice';
import headerReducer from '../components/Header/headerSlice';
import spinnerReducer from '../components/Spinner/spinnerSlice';
import movieDetailReducer from '../features/movieDetail/movieDetailSlice';
import favoriteReducer from '../features/favorite/favoriteSlice';
import movieTableReducer from '../components/MovieTable/movieTableSlice';

export default configureStore({
  reducer: {
    history: historyReducer,
    movieList: movieListReducer,
    header: headerReducer,
    spinner: spinnerReducer,
    movieDetail: movieDetailReducer,
    favorite: favoriteReducer,
    movieTable: movieTableReducer
  },
});
