import React from 'react';

import { useSelector } from 'react-redux';

import { selectSpinner } from './spinnerSlice';

import { Spinner as BsSpinner } from 'react-bootstrap';
import './Spinner.css';

export const Spinner = () => {
    const spinnerState = useSelector(selectSpinner);

    return spinnerState.isShowSpinner
        ? (<BsSpinner animation="grow" />)
        : ``;
}