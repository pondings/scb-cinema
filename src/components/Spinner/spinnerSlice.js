import { createSlice } from '@reduxjs/toolkit';

export const spinnerSlice = createSlice({
    name: 'spinner',
    initialState: { data: {
        isShowSpinner: false
    } },
    reducers: {
        showSpinner: state => { state.data.isShowSpinner = true },
        hideSpinner: state => { state.data.isShowSpinner = false }
    }
});

export const { showSpinner, hideSpinner } = spinnerSlice.actions;

export const selectSpinner = state => state.spinner.data;

export default spinnerSlice.reducer;