import React from 'react';
import { Spinner } from './../Spinner';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

const mockStore = configureMockStore();

describe('Snapshots', () => {
    const initialState = {
        spinner: { data: { isShowSpinner: false } }
    };
    const getMockStore = (state) => mockStore(state);
    const getWrapper = (store) => renderer.create(
        <Provider store={store}>
            <Spinner/>
        </Provider>
    );

    it('Should render Spinner', () => {
        const store = getMockStore(initialState);
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should show spinner', () => {
        const store = getMockStore({ ...initialState, spinner: { data: { isShowSpinner: true } } });
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });
});