import SpinnerReducer, { showSpinner, hideSpinner } from './../spinnerSlice';

describe('Actions', () => {

    it('Should create an action to show spinner', () => {
        expect(showSpinner()).toEqual({ type: showSpinner.type });
    });

    it('Should create an action to hide spinner', () => {
        expect(hideSpinner()).toEqual({ type: hideSpinner.type });
    });

});

describe('Reducers', () => {

    it('Should return initial state', () => {
        expect(SpinnerReducer(undefined, {})).toEqual({ data: { isShowSpinner: false } });
    });

    it('Should handle showSpinner', () => {
        expect(SpinnerReducer(undefined, { type: showSpinner.type }))
            .toEqual({ data: { isShowSpinner: true } });
    });

    it('Should handle hideSpinner', () => {
        expect(SpinnerReducer({ data: { isShowSpinner: true } }, { type: hideSpinner.type }))
            .toEqual({ data: { isShowSpinner: false } });
    });

});