import React from 'react';

import { Button as BsButton } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const Button = ({ variant, className, onClick, text, icon, iconColor, iconClassName }) => {
    return (
        <BsButton variant={variant} className={className} onClick={onClick}>
            {!!icon && <FontAwesomeIcon className={iconClassName} icon={icon} />}
            <span className="pl-1">{text}</span>
        </BsButton>
    );
}