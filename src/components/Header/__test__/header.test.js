import React from 'react';
import { Header } from './../Header';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import renderer from 'react-test-renderer';
import { 
    initialState as headerInitialState,
    movieListState,
    favoriteState,
    movieDetailState,
    historyState
} from './../headerSlice';

const mockStore = configureMockStore();

jest.mock('react-router-dom', () => ({
    useLocation: jest.fn().mockReturnValue(),
    useHistory: jest.fn().mockReturnValue()
}));

describe('Snapshots', () => {
    const initialState = {
        favorite: { data: {} },
        header: { data: headerInitialState }
    };
    const getMockStore = (state) => mockStore(state);
    const getWrapper = (store) => renderer.create(
        <Provider store={store}>
            <Header />
        </Provider>
    )

    it('Should render Header', () => {
        const store = getMockStore(initialState);
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should render header state in history', () => {
        const store = getMockStore({ ...initialState, header: { data: historyState } });
        const wrapper = getWrapper(store).toJSON();
        
        expect(wrapper).toMatchSnapshot();
    });

    it('Should render header state in movie list', () => {
        const store = getMockStore({ ...initialState, header: { data: movieListState } });
        const wrapper = getWrapper(store).toJSON();
        
        expect(wrapper).toMatchSnapshot();
    });

    it('Should render header state in favorite', () => {
        const store = getMockStore({ ...initialState, header: { data: favoriteState } });
        const wrapper = getWrapper(store).toJSON();
        
        expect(wrapper).toMatchSnapshot();
    });

    it('Should render header state in movie detail', () => {
        const store = getMockStore({ ...initialState, header: { data: movieDetailState } });
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });
});