import
HeaderReducer,
{
    goToMovieList,
    goToFavorite,
    goToHistory,
    goToMovieDetail,
    goBack,
    goBackToHistory,
    searchMovies,
    initialState,
    movieListState,
    favoriteState,
    historyState,
    movieDetailState
} from './../headerSlice';
import { addToSearchHistory } from './../../../features/history/historySlice';
import { receiveMovies, clearMovies } from './../../../features/movieList/movieListSlice';
import { showSpinner, hideSpinner } from './../../Spinner/spinnerSlice';
import { resetScrollPosition } from './../../MovieTable/movieTableSlice';

import axios from 'axios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';

jest.mock('axios');
const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Actions', () => {

    it('Should create an action go to movie list', () => {
        expect(goToMovieList()).toEqual({ type: goToMovieList.type });
    });

    it('Should create an action go to favorite', () => {
        expect(goToFavorite()).toEqual({ type: goToFavorite.type });
    });

    it('Should create an action go to history', () => {
        expect(goToHistory()).toEqual({ type: goToHistory.type });
    });

    it('Should create an action go to movie detail', () => {
        expect(goToMovieDetail()).toEqual({ type: goToMovieDetail.type })
    });

});

describe('Reducer', () => {

    it('Should return initial state', () => {
        expect(HeaderReducer(undefined, {})).toEqual({ data: initialState });
    });

    it('Should handle goToMovieList', () => {
        expect(HeaderReducer({ data: initialState }, { type: goToMovieList.type }))
            .toEqual({ data: movieListState })
    });

    it('Should handle goToFavorite', () => {
        expect(HeaderReducer({ data: initialState }, { type: goToFavorite.type }))
            .toEqual({ data: favoriteState });
    });

    it('Should handle goToHistory', () => {
        expect(HeaderReducer({ data: initialState }, { type: goToHistory.type }))
            .toEqual({ data: historyState });
    });

    it('Should handle goToMovieDetail', () => {
        expect(HeaderReducer({ data:initialState }, { type: goToMovieDetail.type }))
            .toEqual({ data: movieDetailState });
    })

});

describe('Thunk Action', () => {
    let store;

    /* Sort by action length */
    const sortActions = (previous, current) => (previous.type.length - current.type.length);

    beforeEach(() => {
        store = mockStore({ data: initialState });
    })
    
    it('Should handle movie list state from movie detail', () => {
        store.dispatch(goBack('/movie-detail'));
        expect(store.getActions()).toEqual([ { type: goToMovieList.type } ]);
    });

    it('Should handle history state from movie-list', () => {
        store.dispatch(goBack('/movie-list'));
        const actionsFromStore = store.getActions().sort(sortActions);
        const expectActions = [
            { type: goToHistory.type, payload: undefined },
            { type: resetScrollPosition.type, payload: undefined }
        ].sort(sortActions);

        expect(actionsFromStore).toEqual(expectActions);
    });

    it('Should handle history state from favorite', () => {
        store.dispatch(goBack('/favorite'));
        const actionsFromStore = store.getActions().sort(sortActions);
        const expectActions = [
            { type: goToHistory.type, payload: undefined },
            { type: resetScrollPosition.type, payload: undefined }
        ].sort(sortActions);

        expect(actionsFromStore).toEqual(expectActions);
    });

    it('Should handle searchMovies', () => {
        const mockData = { total_pages: 2, page: 1, results: [{ title: 'Spider man' }] };
        axios.get.mockResolvedValue({ data: mockData });

        return store.dispatch(searchMovies({ wording: 'Spider man', page: 1 })).then(() => {
            const actionsFromStore = store.getActions().sort(sortActions);
            const expectedActions = [
                { type: addToSearchHistory.type, payload: 'Spider man' },
                { type: showSpinner.type, payload: undefined },
                { type: goToMovieList.type, payload: undefined },
                { type: clearMovies.type, payload: undefined },
                { type: receiveMovies.type, payload: { ...mockData, wording: 'Spider man' } },
                { type: hideSpinner.type, payload: undefined }
            ].sort(sortActions);

            expect(actionsFromStore).toEqual(expectedActions);
        });
    });

    it('Should handle goBackToHistory', () => {
        store.dispatch(goBackToHistory());
        const actionsFromStore = store.getActions().sort(sortActions);
        const expectedActions = [
            { type: resetScrollPosition.type, payload: undefined },
            { type: goToHistory.type, payload: undefined }
        ].sort(sortActions);

        expect(actionsFromStore).toEqual(expectedActions);
    });
});