import React from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { selectHeader, goBack, goBackToHistory, goToFavorite } from './headerSlice';
import { receiveMovies } from './../../features/movieList/movieListSlice';
import { selectFavorite } from './../../features/favorite/favoriteSlice';

/* Components */
import { SearchBox } from './SearchBox';
import { Button } from './../Button';

/* Styles */
import './Header.css';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';

export const Header = () => {
    const dispatch = useDispatch();

    const favoriteState = useSelector(selectFavorite);
    const headerState = useSelector(selectHeader);
    const routeHistory = useHistory();
    const routeLocation = useLocation();

    const handleGoBackClick = (e) => {
        routeHistory.goBack();
        dispatch(goBack(routeLocation.pathname));
    };

    const handleBackToSearchClick = () => {
        routeHistory.push('/');
        dispatch(goBackToHistory());
    }

    const handleFavoriteClick = () => {
        dispatch(receiveMovies(favoriteState));
        routeHistory.push('/favorite');
        dispatch(goToFavorite());
    }

    return (
        <div className="header align-items-center">
            <div className="h-60-px flex-half d-flex align-items-center justify-content-between col-12">
                <div className="h-100 d-flex justify-content-center">
                    {
                        headerState.showBackButton &&
                        <Button
                            variant="link"
                            className="nav-btn pl-0"
                            onClick={handleGoBackClick}
                            text="Back"
                            icon={faChevronLeft} />
                    }
                </div>
                <div className="h-100 d-flex justify-content-center">
                    {
                        headerState.showFavoriteButton &&
                        <Button variant="link" text="Favorite" onClick={handleFavoriteClick} />
                    }
                    {
                        headerState.showBackToSearchButton &&
                        <Button 
                            variant="link" 
                            text="Back to search" 
                            className="nav-btn pr-0" 
                            onClick={handleBackToSearchClick} />
                    }
                </div>
            </div>
            { headerState.showSearchBox && <SearchBox /> }
        </div>
    )
}