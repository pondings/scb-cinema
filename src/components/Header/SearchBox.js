import React, { useState } from 'react';

import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { searchMovies } from './headerSlice';

/* Bootstrap */
import { InputGroup, FormControl, Button } from 'react-bootstrap';

/* Styles */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

export const SearchBox = () => {
    const [showCancel, setShowCancel] = useState(false);
    const [wording, setWording] = useState('');

    const routeHistory = useHistory();
    const dispatch = useDispatch();

    const handleCancelClick = () => {
        setWording('');
        setShowCancel(false);
    }

    const handleBlur = () => {
        if (wording) return;
        setShowCancel(false);
    }

    const handleRoute = (e) => {
        if (e.key === 'Enter' && e.target.value.trim()) {
            if (window.location.pathname === '/') routeHistory.push('/movie-list');
            dispatch(searchMovies({ wording: e.target.value }));
        }
    }

    return (
        <div className="h-60-px flex-half col-12 px-0 px-md-1 d-flex justify-content-center align-items-center">
            <InputGroup size="sm" className="h-100 w-90 justify-content-center align-items-center">
                <div
                    className="w-100 d-flex flex-row align-items-center"
                    onFocus={() => setShowCancel(true)} >
                    <FontAwesomeIcon className="search-icon" icon={faSearch} />
                    <FormControl
                        className="w-100"
                        value={wording}
                        onChange={(e) => setWording(e.target.value)}
                        onKeyPress={handleRoute}
                        onClick={() => setShowCancel(true)} 
                        onBlur={handleBlur} >
                    </FormControl>
                    {
                        showCancel &&
                        <Button
                            variant="link"
                            onClick={handleCancelClick}>
                            Cancel
                        </Button>
                    }
                </div>
            </InputGroup>
        </div>
    )
}