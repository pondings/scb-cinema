import { createSlice } from '@reduxjs/toolkit';

import { addToSearchHistory } from './../../features/history/historySlice';
import { searchMovies as movieListSearchMovies } from './../../features/movieList/movieListSlice';
import { showSpinner, hideSpinner } from './../Spinner/spinnerSlice';
import { resetScrollPosition } from './../MovieTable/movieTableSlice';

export const initialState = {
    showSearchBox: true,
    showBackButton: false,
    showFavoriteButton: true,
    showBackToSearchButton: false
};

export const movieListState = {
    showSearchBox: false,
    showBackButton: true,
    showFavoriteButton: false,
    showBackToSearchButton: false
};

export const historyState = {
    showSearchBox: true,
    showBackButton: false,
    showFavoriteButton: true,
    showBackToSearchButton: false
}

export const movieDetailState = {
    showSearchBox: false,
    showBackButton: true,
    showFavoriteButton: false,
    showBackToSearchButton: true
};

export const favoriteState = {
    showSearchBox: false,
    showBackButton: true,
    showFavoriteButton: false,
    showBackToSearchButton: false
}

export const headerSlice = createSlice({
    name: 'header',
    initialState: { data: initialState },
    reducers: {
        goToMovieList: state => {
            state.data = movieListState;
        },
        goToHistory: state => {
            state.data = historyState;
        },
        goToMovieDetail: state => {
            state.data = movieDetailState;
        },
        goToFavorite: state => {
            state.data = favoriteState;
        }
    }
});

export const { goToHistory, goToMovieList, goToMovieDetail, goToFavorite } = headerSlice.actions;

export const goBackToHistory = () => dispatch => {
    dispatch(resetScrollPosition());
    dispatch(goToHistory());
}

export const goBack = (currentPath) => dispatch => {
    switch (currentPath) {
        case '/movie-detail':
            dispatch(goToMovieList());
            break;
        case '/favorite':
        case '/movie-list':
        default:
            dispatch(goToHistory());
            dispatch(resetScrollPosition());
            break;
    }
}

export const searchMovies = (data) => dispatch => new Promise((resolve, reject) => {
    try {
        dispatch(addToSearchHistory(data.wording));
        dispatch(goToMovieList());
        dispatch(showSpinner());
        dispatch(movieListSearchMovies(data)).then(() => {
            dispatch(hideSpinner());
            return resolve('Success');
        });
    } catch (error) {
        return reject(`Error while dispatch searchMovies: ${error.toString()}`);
    }
});

export const selectHeader = state => state.header.data;

export default headerSlice.reducer;