import MovieTableReducer,
{
    showBottomLoading,
    hideBottomLoading,
    setScrollPosition,
    resetScrollPosition,
    initialState
} from './../movieTableSlice';

describe('Actions', () => {

    it('Should create an action to show bottom loading', () => {
        expect(showBottomLoading()).toEqual({ type: showBottomLoading.type });
    });

    it('Should create an action to hide bottom loading', () => {
        expect(hideBottomLoading()).toEqual({ type: hideBottomLoading.type });
    });
    
    it('Should create an action to set scroll position', () => {
        expect(setScrollPosition()).toEqual({ type: setScrollPosition.type });
    });

    it('Should create an action to reset scroll position', () => {
        expect(resetScrollPosition()).toEqual({ type: resetScrollPosition.type });
    });

});

describe('Reducers', () => {

    it('Should return initial state', () => {
        expect(MovieTableReducer(undefined, {}))
            .toEqual({ data: { isShowBottomLoading: false, scrollPosition: 0 } });
    });

    it('Should handle showBottomLoading', () => {
        expect(MovieTableReducer(undefined, { type: showBottomLoading.type }))
            .toEqual({ data: { ...initialState.data, isShowBottomLoading: true } });
    });

    it('Should handle hideButtomLoading', () => {
        expect(MovieTableReducer(undefined, { type: hideBottomLoading.type }))
            .toEqual({ data: { ...initialState.data, isShowBottomLoading: false } });
    });
    
    it('Should handle setScrollPosition', () => {
        expect(MovieTableReducer(undefined, { type: setScrollPosition.type, payload: 1000 }))
            .toEqual({ data: { ...initialState.data, scrollPosition: 1000 } });
    });

    it('Should handle resetScrollPosition', () => {
        expect(MovieTableReducer(undefined, { type: resetScrollPosition.type }))
            .toEqual({ data: { ...initialState.data, scrollPosition: 0 } });
    });

})