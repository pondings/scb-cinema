import React from 'react';
import { MovieTable } from './../MovieTable';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

const mockStore = configureMockStore();

describe('Snapshots', () => {
    const initialState = {
        movieTable: { data: { results: [] } },
        spinner: { data: { isShowSpinner: false } }
    };
    const getMockStore = (state) => mockStore(state);
    const getWrapper = (store, movieList = []) => renderer.create(
        <Provider store={store}>
            <MovieTable movieList={movieList} />
        </Provider>
    );

    it('Should render Movie Table', () => {
        const store = getMockStore(initialState);
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should render Movie Table and results', () => {
        const movieList =  [ { title: 'Spider man' } ];
        const store = getMockStore(initialState);
        const wrapper = getWrapper(store, movieList).toJSON();

        expect(wrapper).toMatchSnapshot();
    })
});