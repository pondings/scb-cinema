import { createSlice } from '@reduxjs/toolkit';

export const initialState = { data: { isShowBottomLoading: false, scrollPosition: 0 } };

export const movieTableSlice = createSlice({
    name: 'movieTable',
    initialState: initialState,
    reducers: {
        showBottomLoading: state => {
            state.data.isShowBottomLoading = true;
        },
        hideBottomLoading: state => {
            state.data.isShowBottomLoading = false;
        },
        setScrollPosition: (state, action) => {
            state.data.scrollPosition = action.payload;
        },
        resetScrollPosition: (state, action) => {  
            state.data.scrollPosition = 0;
        }
    }
});

export const { showBottomLoading, hideBottomLoading, setScrollPosition, resetScrollPosition } = movieTableSlice.actions;

export const selectMovieTable = state => state.movieTable.data;

export default movieTableSlice.reducer;