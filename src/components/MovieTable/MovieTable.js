import React, { useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { selectSpinner } from './../Spinner/spinnerSlice';
import {
    selectMovieTable,
    showBottomLoading,
    hideBottomLoading,
    setScrollPosition
} from './movieTableSlice';

import { Table, Spinner } from 'react-bootstrap';
import './MovieTable.css';

import * as utils from './../../utils/utils';

export const MovieTable = ({
    movieList,
    handleClick,
    fetchMoreDataFunc,
    notFoundMessage = 'NO MOVIE FOUND'
}) => {
    const dispatch = useDispatch();

    const spinnerState = useSelector(selectSpinner);
    const movieTableState = useSelector(selectMovieTable);

    const myRef = useRef();

    useEffect(() => {
        const containerElement = myRef.current;
        if (containerElement) containerElement.scrollTop = movieTableState.scrollPosition;
    }, [movieTableState.scrollPosition]);

    useEffect(() => {
        /**
         * Fetch more data when reach bottom of table
         */
        const handleScroll = async (e) => {
            if (!fetchMoreDataFunc) return;

            /* Avoid Event spam */
            if (movieTableState.isShowBottomLoading) return;
            const element = e.target;

            if ((element.scrollTop + element.offsetHeight) >= element.scrollHeight) {
                dispatch(showBottomLoading());
                await fetchMoreDataFunc();
                setTimeout(() => {
                    dispatch(hideBottomLoading());
                }, 500);
            }
        };

        const containerElement = myRef.current;
        if (containerElement) {
            containerElement.addEventListener('scroll', handleScroll, false);
        };
        return () => containerElement && containerElement.removeEventListener('scroll', handleScroll);
    }, [spinnerState, fetchMoreDataFunc, movieTableState, dispatch]);

    /**
     * Wait until data ready to display
     */
    if (spinnerState.isShowSpinner) return ``;

    const handleMovieClick = (movie) => {
        handleClick(movie);
        dispatch(setScrollPosition(myRef.current.scrollTop));
    }

    const rowElements = utils.buildMovieDescriptRow(movieList, handleMovieClick);

    /**
     * If empty data will display not found message
     */
    if (rowElements.length < 1) {
        return (
            <div className="movie-list-contaner">
                <span className="not-found">{notFoundMessage}</span>
            </div>
        )
    };

    return (
        <div className="movie-list-contaner" ref={myRef}>
            <Table hover className="movie-list-table">
                <tbody>
                    {rowElements}
                </tbody>
            </Table>
            {
                movieTableState.isShowBottomLoading &&
                <div className="d-flex justify-content-center my-4">
                    <Spinner animation="border" variant="primary" />
                </div>
            }
        </div>
    );
}