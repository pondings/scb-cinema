import { createSlice } from '@reduxjs/toolkit';

export const movieDetailSlice = createSlice({
    name: 'movieDetai',
    initialState: { data: {} },
    reducers: {
        /* Use for set movie to movieDetail's store */
        viewDetail: (state, action) => { state.data = action.payload }
    }
});

export const { viewDetail } = movieDetailSlice.actions;

export const selectMovieDetail = state => state.movieDetail.data;

export default movieDetailSlice.reducer;