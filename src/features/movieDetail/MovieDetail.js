import React from 'react';

import { useSelector, useDispatch } from 'react-redux';

import { selectMovieDetail } from './movieDetailSlice';
import { selectFavorite, addToFavorite, removeFromFavorite } from './../favorite/favoriteSlice';

import { Button } from './../../components/Button';
import './MovieDetail.css';

export const MovieDetail = () => {
    const dispatch = useDispatch();

    const movieDetailState = useSelector(selectMovieDetail);
    const favoriteState = useSelector(selectFavorite);

    /* Check this movie is already favorite */
    const isAlreadFavorite = favoriteState.find(data => data.id === movieDetailState.id);

    /* Add or remove favorite */
    const handleAddFavoriteClick = () => {
        if (!!isAlreadFavorite) {
            dispatch(removeFromFavorite(movieDetailState));
        } else {
            dispatch(addToFavorite(movieDetailState));
        }
    }

    return (
        <div className="col d-flex flex-column movie-detail-content">
            <div className="d-flex w-100-p poster-container justify-content-center">
                <img alt={movieDetailState.title} src={movieDetailState.poster_path} width="138" height="207"></img>
            </div>
            <div className="font-weight-bold mt-4">{movieDetailState.title}</div>
            <div className="mt-2">Average Votes: {movieDetailState.vote_average}</div>
            <div className="pl-4 mt-2">{movieDetailState.overview}</div>
            <div className="mt-5 mt-md-2 mb-5 mb-md-0 flex-1 d-flex align-items-center">
                <Button 
                    text={isAlreadFavorite ? 'Unfavorite' : 'Favorite'} 
                    className="favorite-button h-10"
                    onClick={handleAddFavoriteClick} />
            </div>
        </div>
    );
}