import MovieDetailReducer, { viewDetail } from './../movieDetailSlice';

describe('Actions', () => {

    it('Should to create an action to prepare Movie Detail', () => {
        expect(viewDetail({ title: 'Spider man' }))
            .toEqual({ type: viewDetail.type, payload: { title: 'Spider man' } });
    });

});

describe('Reducers', () => {

    it('Should handle viewDetail', () => {
        expect(MovieDetailReducer(undefined, { type: viewDetail.type, payload: { title: 'Spider man' } }))
            .toEqual({ data: { title: 'Spider man' } });
    });

});