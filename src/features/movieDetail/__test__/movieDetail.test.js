import React from 'react';
import { MovieDetail } from './../MovieDetail';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

const initialState = {
    movieDetail: { data: {} },
    favorite: { data: [] }
};
const mockStore = configureMockStore();

describe('Snapshots', () => {
    const getMockStore = (state) => mockStore(state);
    const getWrapper = (store) => renderer.create(
        <Provider store={store}>
            <MovieDetail />
        </Provider>
    )

    it('Should show movie detail', () => {
        const store = getMockStore(initialState);
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should show movie detail and display an information', () => {
        const store = getMockStore({ ...initialState, movieDetail: { data: { title: 'Spider man', overview: 'Snaps Test' } } });
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    })
});