import { createSlice } from '@reduxjs/toolkit';

export const favoriteSlice = createSlice({
    name: 'favorite',
    initialState: { data: [] },
    reducers: {
        addToFavorite: (state, action) => {
            state.data.unshift(action.payload);
        },
        removeFromFavorite: (state, action) => {
            if (state.data.find(d => d.id === action.payload.id)) {
                state.data = state.data.filter(d => d.id !== action.payload.id);
            }
        }
    }
});

export const { addToFavorite, removeFromFavorite } = favoriteSlice.actions;

export const selectFavorite = state => state.favorite.data;

export default favoriteSlice.reducer;