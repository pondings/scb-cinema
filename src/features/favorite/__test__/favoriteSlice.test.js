import favoriteReducer, { addToFavorite, removeFromFavorite } from '../favoriteSlice';

describe('Action - addToFavorite', () => {
    it('Should create an action to add favorite', () => {
        const movie = { title: 'spider man' };
        const expectedAction = { type: addToFavorite.type, payload: movie };
        expect(addToFavorite(movie)).toEqual(expectedAction);
    });
});

describe('Action - removeFromFavorite', () => {
    it('Should create an action to remove favorite', () => {
        const movie = { title: 'spider man' };
        const expectedAction = { type: removeFromFavorite.type, payload: movie };
        expect(removeFromFavorite(movie)).toEqual(expectedAction);
    });
});

describe('Reducer', () => {
    const supermanMovie = { id: 1, title: 'Superman' };
    const spiderManMovie = { id: 2, title: 'Spider man' };

    it('Should return the initial state', () => {
        expect(favoriteReducer(undefined, {})).toEqual({ data: [] });
    });

    it('Should handle addToFavorite', () => {
        expect(favoriteReducer(undefined, { type: addToFavorite.type, payload: supermanMovie }))
            .toEqual({ data: [supermanMovie] });

        const spiderManMovie = { title: 'Spider man' };
        expect(favoriteReducer({ data: [supermanMovie] }, { type: addToFavorite.type, payload: spiderManMovie }))
            .toEqual({ data: [spiderManMovie, supermanMovie] });
    });

    it('Should handler removeFromFavorite', () => {
        expect(favoriteReducer(undefined, { type: removeFromFavorite.type, payload: spiderManMovie }))
            .toEqual({ data: [] });

        expect(favoriteReducer({ data: [supermanMovie] }, { type: removeFromFavorite.type, payload: supermanMovie }))
            .toEqual({ data: [] });

        expect(favoriteReducer({ data: [supermanMovie] }, { type: removeFromFavorite.type, payload: spiderManMovie }))
            .toEqual({ data: [supermanMovie] });

        expect(favoriteReducer(
            { data: [supermanMovie, spiderManMovie] }, { type: removeFromFavorite.type, payload: supermanMovie }
        )).toEqual({ data: [spiderManMovie] })
    });
});