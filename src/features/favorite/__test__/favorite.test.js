import React from 'react';
import { Favorite } from './../Favorite';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

const mockStore = configureMockStore();

describe('Snapshots', () => {
    const initialState = {
        favorite: { data: [] },
        spinner: { data: [] },
        movieTable: { data: { isShowBottomLoading: false } }
    };
    const getMockStore = (state) => mockStore(state);
    const getWrapper = (store) => renderer.create(
        <Provider store={store}>
            <Favorite />
        </Provider>
    )

    it('Should show favorite', () => {
        const store = getMockStore(initialState);
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should show not found message when to favorite movie', () => {
        const store = getMockStore(initialState);
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should show favorite movie', () => {
        const store = getMockStore({ ...initialState, favorite: { data: [
            { title: 'Spider man' },
            { title: 'Superman' }
        ] } });
        const wrapper = getWrapper(store);

        expect(wrapper).toMatchSnapshot();
    });
});