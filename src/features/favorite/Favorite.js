import React from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { viewDetail } from './../movieDetail/movieDetailSlice';
import { goToMovieDetail } from './../../components/Header/headerSlice';
import { selectFavorite } from './favoriteSlice';

import { MovieTable } from './../../components/MovieTable/MovieTable';

export const Favorite = () => {
    const dispatch = useDispatch();
    const routeHistory = useHistory();
    const favoriteState = useSelector(selectFavorite);

    /**
     * Trigger when click a row element
     * Dispatch viewDetail to set movieDetail state
     * Dispatch goToMovieDetail to set Header state
     */
    const handleViewDetailClick = (movie) => {
        dispatch(viewDetail(movie));
        dispatch(goToMovieDetail());
        routeHistory.push('/movie-detail');
    }

    return (
        <MovieTable 
            movieList={favoriteState} 
            handleClick={handleViewDetailClick} 
            notFoundMessage="YOU HAVE NO FAVORITE MOVIE" />
    );
}