import { createSlice } from '@reduxjs/toolkit';

export const historySlice = createSlice({
    name: 'history',
    initialState: { data: [] },
    reducers: {
        /**
         * Add search text to history
         * If keyword already searched, it will move to top of array
         */
        addToSearchHistory: (state, action) => { 
            state.data.push(action.payload);
            state.data = state.data.filter(d => d !== action.payload);
            state.data.unshift(action.payload);
        }
    }
});

export const { addToSearchHistory } = historySlice.actions;

export const selectHistory = state => state.history.data;

export default historySlice.reducer;