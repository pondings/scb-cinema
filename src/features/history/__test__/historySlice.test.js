import historyReducer, { addToSearchHistory } from './../historySlice';

describe('Actions', () => {

    it('Should create an action to add history', () => {
        expect(addToSearchHistory('Spider man'))
            .toEqual({ type: addToSearchHistory.type, payload: 'Spider man' });
    });

});

describe('Reducers', () => {

    it('Should handle addToSearchHistory', () => {
        expect(historyReducer(undefined, { type: addToSearchHistory.type, payload: 'Spider man' }))
            .toEqual({ data: ['Spider man'] });

        expect(historyReducer({ data: ['Spider man'] }, { type: addToSearchHistory.type, payload: 'Superman' }))
            .toEqual({ data: ['Superman', 'Spider man'] });
    });

});