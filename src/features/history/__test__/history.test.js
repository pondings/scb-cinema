import React from 'react';
import { History } from './../History';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

const mockStore = configureMockStore();

describe('Snapshots', () => {
    const getMockStore = (state) => mockStore(state);
    const getWrapper = (store) => renderer.create(
        <Provider store={store}>
            <History />
        </Provider>
    )

    it('Should show history', () => {
        const store = getMockStore({ history: { data: [] } });
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should show spider man in history', () => {
        const store = getMockStore({ history: { data: ['Spider man'] } });
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should show spider man and batman in history', () => {
        const store = getMockStore({ history: { data: ['Spider man', 'Superman'] } });
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });
});