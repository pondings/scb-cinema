import React from 'react';

import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { searchMovies } from './../../components/Header/headerSlice';
import { selectHistory } from './historySlice';

import { Table } from 'react-bootstrap';
import './History.css';

export const History = () => {
    const historyList = useSelector(selectHistory);
    const dispatch = useDispatch();
    const routeHistory = useHistory();

    const handleHistoryClick = history => {
        dispatch(searchMovies(history));
        routeHistory.push('/movie-list');
    }

    const historyListAsRowElement = historyList.map((history, idx) => (
        <tr key={`${history}-${idx}`} onClick={() => handleHistoryClick({ wording: history })}>
            <td>{history}</td>
        </tr>
    ));

    return (
        <Table className="history-table" hover>
            <tbody>
                {historyListAsRowElement}
            </tbody>
        </Table>
    )
}