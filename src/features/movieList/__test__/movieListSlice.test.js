import
MovieListReducer,
{
    receiveMovies,
    appendMovies,
    clearMovies,
    searchMovies,
    searchMoreMovies
} from './../movieListSlice';

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';

const spiderManMovie = { title: 'Spider man' };
const supermanMovie = { title: 'Superman' };
const aloisMovie = { title: 'Alois' };

const movies = [spiderManMovie, supermanMovie, aloisMovie]

jest.mock('axios');
const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Async Thunk Actions', () => {
    let store;

    /* Sort by action length */
    const sortActions = (previous, current) => (previous.type.length - current.type.length);

    beforeEach(() => {
        store = mockStore();
    });

    it('Should handle searchMovies', () => {
        const mockData = { total_pages: 2, page: 1, results: [{ title: 'Spider man' }] };
        axios.get.mockResolvedValue({ data: mockData });

        return store.dispatch(searchMovies({ wording: 'Spider man' })).then(() => {
            const actionsFromStore = store.getActions().sort(sortActions);
            const expectedActions = [
                { type: clearMovies.type, payload: undefined },
                { type: receiveMovies.type, payload: { ...mockData, wording: 'Spider man' } }
            ].sort(sortActions);

            expect(actionsFromStore).toEqual(expectedActions);
        });
    });

    it('Should handle searchMoreMovies', () => {
        const mockData = { total_pages: 2, page: 2, results: [{ title: 'Spider man 2' }] };
        axios.get.mockResolvedValue({ data: mockData });

        return store.dispatch(searchMoreMovies({ wording: 'Spider man', page: 2 })).then(() => {
            const actionsFromStore = store.getActions().sort(sortActions);
            const expectedActions = [
                { type: appendMovies.type, payload: { ...mockData, wording: 'Spider man' } }
            ];

            expect(actionsFromStore).toEqual(expectedActions);
        });
    });

});

describe('Actions', () => {

    it('Should to create an action to receive movies', () => {
        expect(receiveMovies(movies))
            .toEqual({ type: receiveMovies.type, payload: movies });
    });

    it('Should to create an action to clear movies', () => {
        expect(clearMovies())
            .toEqual({ type: clearMovies.type });
    });

    it('Should to create an action to appendMovies', () => {
        expect(appendMovies(movies))
            .toEqual({ type: appendMovies.type, payload: movies });
    });

});

describe('Reducers', () => {

    it('Should handle receiveMovies', () => {
        expect(MovieListReducer(undefined, { type: receiveMovies.type, payload: { results: [] } }))
            .toEqual({ data: { results: [] } });

        expect(MovieListReducer(undefined, { type: receiveMovies.type, payload: { results: movies } }))
            .toEqual({ data: { results: movies } });

        expect(MovieListReducer(
            { data: { results: [{ title: 'Spider man' }] } }, { type: receiveMovies.type, payload: { results: movies } })
        ).toEqual({ data: { results: movies } });
    });

    it('Should handle clear movies', () => {
        expect(MovieListReducer(undefined, { type: clearMovies.type }))
            .toEqual({ data: { results: [] } });

        expect(MovieListReducer({ data: { results: [movies] } }, { type: clearMovies.type }))
            .toEqual({ data: { results: [] } });
    });

    it('Should handle append movies', () => {
        expect(MovieListReducer(undefined, { type: appendMovies.type, payload: { results: [] } }))
            .toEqual({ data: { results: [] } });

        expect(MovieListReducer(undefined, { type: appendMovies.type, payload: { results: movies } }))
            .toEqual({ data: { results: movies } });

        expect(MovieListReducer(
            { data: { results: movies } }, { type: appendMovies.type, payload: { results: [{ title: 'Kingman' }] } })
        ).toEqual({ data: { results: [...movies, { title: 'Kingman' }] } });
    });

});