import React from 'react';
import { MovieList } from './../MovieList';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

const initialState = {
    movieList: { data: {} },
    spinner: { data: { isShowSpinner: false } },
    movieTable: { data: [] }
};
const mockStore = configureMockStore();

describe('Snapshots', () => {
    const getMockStore = (state) => mockStore(state);
    const getWrapper = (store) => renderer.create(
        <Provider store={store}>
            <MovieList />
        </Provider>
    )

    it('Should show empty movie list', () => {
        const store = getMockStore(initialState);
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    });

    it('Should show movie list', () => {
        const movieList = { data: { results: [
            { title: 'Spider man' },
            { title: 'Super man' }
        ] } };
        const store = getMockStore({ ...initialState, movieList });
        const wrapper = getWrapper(store).toJSON();

        expect(wrapper).toMatchSnapshot();
    })
});