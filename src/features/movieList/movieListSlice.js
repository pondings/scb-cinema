import { createSlice } from '@reduxjs/toolkit';
import * as movieApi from './../../api/movie';
import * as utils from './../../utils/utils';

export const movieListSlice = createSlice({
    name: 'movieList',
    initialState: { data: { results: [] } },
    reducers: {
        receiveMovies: (state, action) => { state.data = action.payload },
        clearMovies: state => { state.data = { results: [] } },
        appendMovies: (state, action) => {
            state.data = { ...action.payload, results: [...state.data.results, ...action.payload.results] }
        }
    }
});

export const { receiveMovies, clearMovies, appendMovies } = movieListSlice.actions;

export const selectMovieList = state => state.movieList.data;

export const searchMoreMovies = ({ wording, page }) => dispatch => new Promise(async (resolve, reject) => {
    try {
        let data = await movieApi.fetchMovies(wording, page);
        data.results = data.results.map(utils.populatePosterPath);
        dispatch(appendMovies({ ...data, wording, page }));

        return resolve(data);
    } catch (error) {
        return reject(`Error while searchMoreMovies: ${error.toString()}`);
    }
});

export const searchMovies = ({ wording, page = 1 }) => dispatch => new Promise(async (resolve, reject) => {
    try {
        dispatch(clearMovies());
        let data = await movieApi.fetchMovies(wording, page);
        data.results = data.results.map(utils.populatePosterPath);

        dispatch(receiveMovies({ ...data, wording, page }));
        return resolve(data);
    } catch (error) {
        return reject(`Error while searchMovies: ${error.toString()}`);
    }
});

export default movieListSlice.reducer;