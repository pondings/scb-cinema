import React from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { selectMovieList, searchMoreMovies } from './movieListSlice';
import { viewDetail } from './../movieDetail/movieDetailSlice';
import { goToMovieDetail } from './../../components/Header/headerSlice';

import { MovieTable } from './../../components/MovieTable/MovieTable';

export const MovieList = () => {
    const movieListState = useSelector(selectMovieList);
    const routeHistory = useHistory();
    const dispatch = useDispatch();

    /**
     * Trigger when click a row element
     * Dispatch viewDetail to set movieDetail state
     * Dispatch goToMovieDetail to set Header state
     */
    const handleViewDetailClick = (movie) => {
        dispatch(viewDetail(movie));
        dispatch(goToMovieDetail());
        routeHistory.push('/movie-detail');
    }

    /**
     * Fetch movie next page
     */
    let currentPage = 1;
    const fetchMoreMovies = () => new Promise(async (resolve, reject) => {
        try {
            if (movieListState.total_pages === movieListState.page) return resolve('Already last page');
            const nextPage = movieListState.page + 1;
            if (nextPage === currentPage) return resolve('Still executeing');
            currentPage = nextPage;
            await dispatch(searchMoreMovies({ wording: movieListState.wording, page: nextPage }));

            return resolve('Success');
        } catch (error) {
            return reject(`Error while handleScroll: ${error.toString()}`);
        }
    });

    return (
        <MovieTable
            movieList={movieListState.results}
            handleClick={handleViewDetailClick}
            fetchMoreDataFunc={fetchMoreMovies} />
    )
}